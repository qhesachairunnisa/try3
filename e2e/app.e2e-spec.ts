import { MyApp3Page } from './app.po';

describe('my-app3 App', () => {
  let page: MyApp3Page;

  beforeEach(() => {
    page = new MyApp3Page();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
